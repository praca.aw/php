<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Carbon\Carbon;

class CalculateResponseTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateResponseTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates response time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $bookings = BookingModel::with('office')->get()->toArray();
        
        $responseTimeCalculator = new ResponseTimeCalculator();
        
        $days = [0 => 'Mon', 1 => 'Tue', 2 => 'Wed', 3 => 'Thu', 4 => 'Fri', 5 => 'Sat', 6 => 'Sun'];
        
        foreach ($bookings as $booking) {
            $office = $booking['office'];
            
            $bookingDateTime = $booking['created_at'];
            $responseDateTime = $booking['updated_at'];
            $officeHours = $booking['office']['office_hours'];
            
            $times = $responseTimeCalculator->calculate($bookingDateTime, $responseDateTime, $officeHours);
        
            $booking = new Carbon($bookingDateTime);
            $response = new Carbon($responseDateTime);
            
            $this->line('created_at: ' . $booking->format('Y-m-d H:i:s D'));
            $this->line('updated_at: ' . $response->format('Y-m-d H:i:s D'));
            $this->line('Opening hours:');
            foreach ($days as $day => $name) {
                $hours = $officeHours[$day];
                if ( $hours['isClosed'] === true ) {
                    $time = 'closed';
                } else {
                    $time = $hours['from'] . ' - ' . $hours['to'];
                }
                
                $this->line("\t" . $days[$day] . ': ' . $time);
            }
            
            $this->line('');
            
            $minutes = 0;
            $text = [];
            foreach ($times as $time) {
                $minutes += (int) $time['minutes'];
                $text[] = $time['minutes'] . ' minutes on ' . $time['date'];
            }
            
            $text = implode(' + ', $text);
            
            $text .= ' = ' . $minutes . ' minutes.';
            
            $this->line('Response time will be: ' . $text);
        }
    }
}
