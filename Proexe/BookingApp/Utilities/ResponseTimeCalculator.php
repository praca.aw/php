<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use Carbon\Carbon;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

    public function calculate( $bookingDateTime, $responseDateTime, $officeHours ) {
        $booking = new Carbon($bookingDateTime);
        $response = new Carbon($responseDateTime);
        
        $calcs = [];
       
        $isFirstDay = true;
        $ifLastDay = false;
        
        while ($booking->timestamp < $response->timestamp) {
            $hours = $officeHours[$booking->dayOfWeek];
            if ( $hours['isClosed'] === true ) {
                $booking->addDays(1);
                $isFirstDay = false;
                continue;
            }
            
            $isLastDay = ($booking->diffInDays($response) === 0);
            
            list($hour, $minute) = explode(':', $hours['from']);
            $opening = $booking->copy();
            $opening->hour((int) $hour)
                    ->minute((int) $minute)
                    ->second(0);

            list($hour, $minute) = explode(':', $hours['to']);
            $closing = $booking->copy();
            $closing->hour((int) $hour)
                    ->minute((int) $minute)
                    ->second(0);

            if ( $isFirstDay === true ) {
                if ( $booking->hour > $opening->hour ) {
                    $opening->hour($booking->hour)
                            ->minute($booking->minute)
                            ->second($booking->second);
                }
            } 

            if ( $isLastDay === true ) {
                if ( $response->hour < $closing->hour ) {
                    $closing->hour($response->hour)
                            ->minute($response->minute)
                            ->second($response->second);
                }
            }
            
            $calcs[] = ['minutes' => $closing->diffInMinutes($opening), 'date' => $booking->format('D, d M Y')];
                
            $booking->addDays(1);
            $isFirstDay = false;
        }
        
        return $calcs;
    }

}