<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {            
            $lat1 = (double) $from[0];
            $lon1 = (double) $from[1]; 
            $lat2 = (double) $to[0]; 
            $lon2 = (double) $to[1];
            
            // Source: https://stackoverflow.com/questions/37876166/haversine-distance-calculation-between-two-points-in-laravel
            // https://www.movable-type.co.uk/scripts/latlong.html
            // Verified: https://www.nhc.noaa.gov/gccalc.shtml
            
            $theta = $lon1 - $lon2; 
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
            $dist = acos($dist); 
            $dist = rad2deg($dist); 
            $miles = $dist * 60 * 1.1515;
            
            $distance = 0.0;
            switch ($unit) {
                case 'm': // meters
                    $distance = ($miles * 1.609344) / 1000;
                    break;
                case 'km': // kilometers
                    $distance = ($miles * 1.609344);
                    break;
            }
            
            return $distance;
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
            $distance = 0.0;
            $office = null;
            foreach ($offices as $calcOffice) {
                $to = [$calcOffice['lat'], $calcOffice['lng']];
                
                $calcDistance = $this->calculate($from, $to, 'km');
                if ( $distance === 0.0 || ( $calcDistance < $distance ) ) {
                    $distance = $calcDistance;
                    $office = $calcOffice;
                }
            }
            
            if ( $office !== null ) {
                return $office['name'];
            }
            
            
            /*
             * Oczywiście proponowałbym użycie funkcji / procedury w bazie danych, 
             * ale ostatecznie można użyć np. uparametryzowanego zapytania jak poniżej.
             * 
             * MYSQL:
             *  select
                    (((acos(sin((:lat*pi()/180)) * sin((o.lat*pi()/180))+cos((:lat*pi()/180)) 
                     * cos((o.lat*pi()/180)) * cos(((:lng - o.lng)*pi()/180))))*180/pi())*60*1.1515) 
                     AS distance
                from offices o
                order by 1 asc
                limit 0, 1
             * 
             * Source: https://stackoverflow.com/questions/1006654/fastest-way-to-find-distance-between-two-lat-long-points
             */

            return 'Not found';
	}

}